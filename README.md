# vjapCHIP-med
files for 3D printable case for CHIP and HDMI DIP inspired by the classic, best-selling computer.

# Description
Inspired by the classic, best-selling computer, C(HIP)64 is a case for your CHIP with HDMI DIP. It all snaps together - no screws required. Print yourself one today! The top half of the case requires support material. I recommend printing at 0.2mm layer height or better!

# License
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>. 